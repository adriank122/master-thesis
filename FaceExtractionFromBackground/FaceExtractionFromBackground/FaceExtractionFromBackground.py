import cv2
import glob
import dlib
from imutils import face_utils

predictor_path = "model/shape_predictor_68_face_landmarks.dat"

face_detector = dlib.get_frontal_face_detector()
landmarks_predictor = dlib.shape_predictor(predictor_path)

#paths = glob.glob('C:\\Users\\Adrian\\Desktop\\MGR\\SEM I\\TestPhotos\\photos\\segregated\\3D\\Andrii\\*')
for path in glob.glob('data/*'):
    frame = cv2.imread(path)
    
    detectedFaces = face_detector(frame, 1)
    for (i, rect) in enumerate(detectedFaces):
        cv2.rectangle(frame, (rect.left(), rect.top()), (rect.right(), rect.bottom()), (255,0,0), 5)
        shape = landmarks_predictor(frame, rect)
        shape = face_utils.shape_to_np(shape)
        for (x,y) in shape:
            cv2.circle(frame, (x,y), 10, (0,0,255), -1)

    cv2.namedWindow("Photo", cv2.WINDOW_NORMAL)
    cv2.imshow("Photo", frame)
    cv2.waitKey(0)
#print(paths)