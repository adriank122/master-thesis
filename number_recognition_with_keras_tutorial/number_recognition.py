import numpy as np

from keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

numberOfPicture = 45


import matplotlib.pyplot as plt
plt.imshow(X_test[numberOfPicture])
plt.show()

from keras.models import load_model

model = load_model('number_recognition_model.h5')

from keras.preprocessing import image 

X_test = image.img_to_array(X_test)
temp = np.resize(X_test[numberOfPicture], (1, X_test[numberOfPicture].shape[0], X_test[numberOfPicture].shape[1], 1))
#X_test[0] = np.expand_dims(X_test[0], axis=0)
#X_test[0] = np.resize(X_test[0], (-1,28,28))
#X_test[0] = np.concatenate(X_test[0], axis=0)

#X_train = image.img_to_array(X_train)
#X_test = np.expand_dims(X_test, axis=0)

result = model.predict(temp)

print(result)