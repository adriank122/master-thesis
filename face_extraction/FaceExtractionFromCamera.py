import numpy as np 
import cv2

cap = cv2.VideoCapture(0)

numberOfFace = 0

while(True):
    ret, frame = cap.read()

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    numberOfFace += 1

    for (x,y,w,h) in faces:
        #cv2.rectangle(frame, (x,y), (x+w, y+h), (255,0,0), 2)
        cropped = frame[y:y+h, x:x+w]
        cv2.imshow('cropped', cropped)
        #cv2.imwrite("faces\\face" + str(numberOfFace) + ".jpg", cropped)
        

    cv2.imshow('output', frame)
    if(cv2.waitKey(100) == 32):
        break

cap.release()
cv2.destroyAllWindows()