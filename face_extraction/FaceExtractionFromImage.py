import numpy as np 
import cv2
import time

numberOfFace = 0
numberOfFace2 = 0

t = time.time()

while(True):

    numberOfFace += 1

    path = "C:\\Users\\Acer\\Desktop\\SEMI\\MGR\\CNNdata\\Adrian\\" + str(numberOfFace) + ".jpg"
    image = cv2.imread(path)

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x,y,w,h) in faces:
        numberOfFace2 += 1
        cropped = image[y:y+h, x:x+w]
        #cv2.imshow('cropped', cropped)
        cv2.imwrite("D:\\faces\\Adrian\\" + str(numberOfFace2) + ".jpg", cropped)

elapsed = time.time() - t
print(elapsed)
