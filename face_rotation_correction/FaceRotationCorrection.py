import sys
import os
import dlib
import glob
import cv2
from imutils import face_utils
import numpy as np
import math

predictor_path = "model/shape_predictor_68_face_landmarks.dat"

faceDetector = dlib.get_frontal_face_detector()
landmarksPredictor = dlib.shape_predictor(predictor_path)

cap = cv2.VideoCapture(0)

dstPoints = None

#36, 45, 57 -> outside eyes and chick 

while cap is not None:
    _, frame = cap.read()
    detectedFaces = faceDetector(frame, 1)
    for (i, rect) in enumerate(detectedFaces):
        cv2.rectangle(frame, (rect.left(), rect.top()), (rect.right(), rect.bottom()), (255,0,0), 2)
        shape = landmarksPredictor(frame, rect)
        shape = face_utils.shape_to_np(shape)
        for (x,y) in shape:
            cv2.circle(frame, (x,y), 2, (0,0,255), -1)
            angle = ((shape[36][0] + shape[45][0])/2 - shape[57][0])/math.sqrt(((shape[36][0] + shape[45][0])/2 - shape[57][0])**2 + (shape[57][1] - (shape[36][1] + shape[45][1])/2)**2)
            angle = angle * 180 / math.pi
            print(angle)
            if angle is not 0.0:
                M = cv2.getRotationMatrix2D((shape[57][0], shape[57][1]), angle, 1)
                rows, cols, _ = frame.shape
                transformedFrame = cv2.warpAffine(frame, M, (cols,rows))
    cv2.imshow("Transformed Image", transformedFrame)
    cv2.imshow("Face Landmarks", frame)
    if cv2.waitKey(10) == 27:
        break

cap.release()